<?php

/**
 * Theme function for our select widget.
 */
function theme_dynamic_formatters_select($element) {
  return $element['#children'];
}

/**
 * Theme function for default formatter.
 *
 * @todo Check for allowed values.
 */
function theme_dynamic_formatters_formatter_default($element) {
  return $element['#item']['safe'];
}

/**
 * Theme function for plain formatter.
 *
 * @todo Check for allowed values.
 */
function theme_dynamic_formatters_formatter_plain($element) {
  return strip_tags($element['#item']['safe']);
}

/**
 * Theme function for the views style plugin.
 *
 * @todo
 *   Preprocess this function to remove some logics.
 *
 * @todo
 *   Wrap the inclution and creation of the DynamicView class in a separate
 *   function.
 */
function theme_dynamic_formatters_view_rows($view, $options, $rows, $title) {
  return $view->style_plugin->dynamic_formatters_renderer->render_rows($rows);
}
