<?php

/**
 * Implementation of hook_views_handler().
 */
function dynamic_emfield_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'dynamic_emfield') . '/views/handlers',
    ),
    'handlers' => array(
      'dynamic_emimage_handler_field_multiple' => array(
        'parent' => 'content_handler_field_multiple',
        'file' => 'dynamic_emimage_handler_field_multiple.inc',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 *
 * Switch field handler for all emfields.
 */
function dynamic_emfield_views_data_alter(&$data) {
  $fields = content_fields();
  foreach ($fields as $field_name => $field) {
    if ($field['type'] == 'emimage') {
      $table_alias = content_views_tablename($field);
      $data[$table_alias][$field_name . '_embed']['field']['handler'] = 'dynamic_emimage_handler_field_multiple';
    }
  }
}
