<?php

class dynamic_emimage_handler_field_multiple extends content_handler_field_multiple {

  function options(&$options) {
    parent::options($options);
    $options['dynamic_emimage'] = FALSE;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['dynamic_emimage'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use dynamic formatter'),
      '#description' => t('If checked, this field will use a formatter provided by a dynamic views style.'),
      '#default_value' => $this->options['dynamic_emimage'],
      '#weight' => 5,
    );
  }

  function render($values) {
    // Shall we use dynamic ImageCache settings for this field?
    if ($this->options['dynamic_emimage']) {
      $this->options['format'] = $this->view->style_plugin->dynamic_formatters_renderer->rows[$values->nid]['style']['formatters']['emimage']['format'];
    }
    return parent::render($values);
  }
}
