<?php

/**
 * Implementation of hook_views_handler().
 */
function dynamic_filefield_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'dynamic_filefield') . '/views/handlers',
    ),
    'handlers' => array(
      'dynamic_filefield_handler_field_multiple' => array(
        'parent' => 'content_handler_field_multiple',
        'file' => 'dynamic_filefield_handler_field_multiple.inc',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 *
 * Switch field handler for all filefields.
 */
function dynamic_filefield_views_data_alter(&$data) {
  $fields = content_fields();
  foreach ($fields as $field_name => $field) {
    if ($field['type'] == 'filefield') {
      $table_alias = content_views_tablename($field);
      $data[$table_alias][$field_name . '_fid']['field']['handler'] = 'dynamic_filefield_handler_field_multiple';
    }
  }
}
