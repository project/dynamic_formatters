<?php

class dynamic_filefield_handler_field_multiple extends content_handler_field_multiple {

  function options(&$options) {
    parent::options($options);
    $options['dynamic_filefield'] = FALSE;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['dynamic_filefield'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use dynamic formatter'),
      '#description' => t('If checked, this field will use a formatter provided by a dynamic views style.'),
      '#default_value' => $this->options['dynamic_filefield'],
      '#weight' => 5,
    );
  }

  /**
   * Render callback.
   *
   * @todo Do not hard code $values->nid, use the primary field
   * (with relationships?) instead.
   */
  function render($values) {
    if ($this->options['dynamic_filefield']) {
      $this->options['format'] = $this->view->style_plugin->dynamic_formatters_renderer->rows[$values->nid]['style']['formatters']['filefield']['format'];
    }
    return parent::render($values);
  }
}
