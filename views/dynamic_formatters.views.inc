<?php

/**
 * Implementation of hook_views_plugins().
 *
 * @todo Create a fields style plugin. For now Semantic Views will do it.
 */
function dynamic_formatters_views_plugins() {
  $path = drupal_get_path('module', 'dynamic_formatters');
  return array(
    'style' => array(
      'dynamic_formatters_rows' => array(
        'title' => t('Dynamic style'),
        'help' => t('Renders dynamic rows one after another.'),
        'handler' => 'dynamic_formatters_plugin_style_rows',
        'path' => $path . '/views/handlers',
        'theme' => 'dynamic_formatters_view_rows',
        'theme file' => 'dynamic_formatters.theme.inc',
        'theme path' => $path . '/theme',
        'uses row plugin' => TRUE,
         'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_pre_render().
 *
 * Before we build the view, we have to sort out what styles to apply to
 * each row. So we attach our dynamic views style renderer here.
 *
 * @todo
 *   Find a way to detect and use the primary key. We wan't to have this module
 *   working on other base tables than node also.
 */
function dynamic_formatters_views_pre_render(&$view) {
  // Do nothing if this isn't a view with the dynamic view style plugin.
  if ($view->style_plugin->definition['handler'] != 'dynamic_formatters_plugin_style_rows') {
    return NULL;
  }

  // Attach the renderer to the style plugin here.
  module_load_include('inc', 'dynamic_formatters', 'includes/dynamic_formatters_renderer');
  $view->style_plugin->dynamic_formatters_renderer = new dynamic_formatters_renderer($view->style_plugin->options);

  // Find out if we will use a global style object for the whole view or not.
  $style_name = $view->style_plugin->options['style_name'];

  // We have separate loops for each case for small performance gain.
  if (!empty($style_name)) {
    foreach ($view->result as $i => $fields) {
      $key = $fields->nid;
      $view->style_plugin->dynamic_formatters_renderer->prepare_row($key, $style_name);
    }
  }
  // We have one style object for each row.
  else {
    // Get the field handlers that we need to work with.
    $style_field = $view->field[$view->style_plugin->options['style_field']];
    $parent_field = $view->field[$view->style_plugin->options['parent_field']];

    foreach ($view->result as $i => $fields) {
      $style_name = isset($fields->{$style_field->field_alias}) ? $fields->{$style_field->field_alias} : NULL;
      $parent_key = isset($fields->{$parent_field->field_alias}) ? $fields->{$parent_field->field_alias} : NULL;
      $key = $fields->nid;
      $view->style_plugin->dynamic_formatters_renderer->prepare_row($key, $style_name, $parent_key);
    }
  }
  $view->style_plugin->dynamic_formatters_renderer->build_rows();
}
