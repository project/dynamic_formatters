<?php

class dynamic_formatters_plugin_style_rows extends views_plugin_style {
  /**
   * Define default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['style_name'] = '';
    $options['style_field'] = '';
    $options['parent_field'] = '';

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $styles = dynamic_formatters_get_styles();
    $style_options = array('' => '- None -');
    foreach ($styles as $style_name => $style) {
      $style_options[$style_name] = $style['title'];
    }
    $form['style_name'] = array(
      '#type' => 'select',
      '#title' => 'Style',
      '#description' => t('Select which style that will to render this view.'),
      '#options' => $style_options,
      '#default_value' => $this->options['style_name'],
    );

    // Get all available fields in this view.
    $handlers = $this->display->handler->get_handlers('field');

    $field_options = array('' => '- None -');
    $parent_options = array('' => '- None -');
    foreach ($handlers as $field_id => $handler) {
      $field_title = $handler->label() ? $handler->label() : $handler->ui_name();
      $parent_options[$field_id] = $field_title;
      if ($handler->content_field['type'] == 'dynamic_formatters_reference') {
        $field_options[$field_id] = $field_title;
      }
    }

    $form['style_field'] = array(
      '#type' => 'select',
      '#title' => 'Style reference field',
      '#description' => t('Select which field that will provide the reference to a style object that will render each row.'),
      '#options' => $field_options,
      '#default_value' => $this->options['style_field'],
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-style-options-style-name' => array('')
      ),
    );

    $form['parent_field'] = array(
      '#type' => 'select',
      '#title' => 'Parent reference field',
      '#description' => t('Select which field that will tell row to inherit the style object from.'),
      '#options' => $parent_options,
      '#default_value' => $this->options['parent_field'],
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-style-options-style-name' => array('')
      ),
    );
  }

  /**
   * Validate the options form.
   */
  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);
  }
}
