<?php

/**
 * @file
 * This is the render class for a dynamic views style.
 *
 * @todo
 *   Find a better way to handler the group wrapping.
 */


class dynamic_formatters_renderer {
  /**
   * Holds options.
   */
  public $options = array();

  /**
   * Holds all raw row data.
   */
  public $rows = array();

  /**
   * Keeps track on which row index we're at.
   */
  public $global_index = 0;

  /**
   * Keeps track on which index we're at, in a specific group keyed by the
   * parent.
   */
  public $group_index = array();

  /**
   * Holds a map between row indexes and row keys.
   */
  public $index_map = array();

  /**
   * Keeps track of the last row keyed by parent.
   */
  public $last = array();

  /**
   * Tells if all the rows are built or not.
   */
  public $built = FALSE;

  /**
   * Constructor.
   *
   * @todo
   *   Is it cleaner to pass in a complete style object here?
   */
  function __construct($options = array()) {
    $this->options = $options;
  }

  /**
   * Builds all the rows. This function does the heavy lifting with finding out
   * which styles to inherit and use.
   *
   * @todo
   *   What happens if the key doesn't exist in the row?
   *
   * @todo
   *   What happens if the parent isn't present in the view?
   */
  public function &build_row($key, $reset = FALSE) {
    if (!$this->rows[$key]['built'] || $reset) {
      // We have one global style object for the whole view.
      if (!empty($this->options['style_name'])) {
        // Load the style object.
        $style = dynamic_formatters_style_load($this->options['style_name']);

        // Is it the first in the view? If so, override previous style.
        if ($this->is_first() && isset($style['first'])) {
          $this->rows[$key]['style'] = $style['first'];
        }
        // Find out what style to apply.
        // Is the row of an odd index in the view?
        elseif ($this->is_odd() && isset($style['odd'])) {
          $this->rows[$key]['style'] = $style['odd'];
        }
        // If not odd, it's even.
        elseif (isset($style['even'])) {
          $this->rows[$key]['style'] = $style['even'];
        }
        // Still no style? Aplly the default style.
        if (empty($this->rows[$key]['style'])) {
          $this->rows[$key]['style'] = $style['default'];
        }

        // Move the index forward.
        $this->move_index();
      }
      // Each row has it's own style object. But this row has a parent that it
      // will inherit the style object from.
      elseif (!empty($this->rows[$key]['parent'])) {
        $parent = $this->build_row($this->rows[$key]['parent']);
        $parent_key = $this->rows[$key]['parent'];
        // This row inherits the style object from it's parent.
        $this->rows[$key]['style_name'] = $parent['style_name'];
        // Load the parent's style object.
        $style = dynamic_formatters_style_load($parent['style_name']);

        // Find out what style to apply.
        // Is the row of an odd index in the parent's group?
        if ($this->is_odd($parent_key) && isset($style['odd'])) {
          $this->rows[$key]['style'] = $style['odd'];
        }
        // If not odd, it's even.
        elseif (isset($style['even'])) {
          $this->rows[$key]['style'] = $style['even'];
        }
        // Still no style? Aplly the default style.
        if (empty($this->rows[$key]['style'])) {
          $this->rows[$key]['style'] = $style['default'];
        }
        // Save this as the last children for this parent. Later children will
        // override this.
        $this->last[$parent_key] = $key;

        // Move the index forward.
        $this->move_index($parent_key);
      }
      // Each row has it's own style object. This row doesn't have a parent, so
      // it must be first in it's own group.
      else {
        // Load it's style object.
        $style = dynamic_formatters_style_load($this->rows[$key]['style_name']);

        // If the style has a 'first' property, apply it.
        if (isset($style['first'])) {
          $this->rows[$key]['style'] = $style['first'];
        }
        // Still no style? Aplly the default style.
        if (empty($this->rows[$key]['style'])) {
          $this->rows[$key]['style'] = $style['default'];
        }

        // The last children is the parent it self, to start with.
        $this->last[$key] = $key;

        // Move the index forward.
        $this->move_index($key);
      }

      // Always add the group data to the row's style. We don't know which row
      // that will be the last one in a group until we actually will render it.
      // So it always needs to be accessible.
      $this->rows[$key]['style']['group'] = $style['group'];
      $this->rows[$key]['built'] = TRUE;
    }
    return $this->rows[$key];
  }

  /**
   * Processes all rows.
   *
   * @todo
   *   Is it worth having a separate loop for this? Maybe this shall go
   *   directly into $this->render()?
   *
   * @todo
   *   Is it ok to have a static cache here? Or should we implement a
   *   class member for this?
   */
  public function build_rows($reset = FALSE) {
    if (!$this->built || $reset) {
      $this->reset_index();

      foreach ($this->rows as $key => $row) {
        // The index is moved inside this function.
        $this->build_row($key);
      }
      $this->built = TRUE;
    }
  }

  /**
   * Returns the current index.
   */
  public function &get_index($key = NULL) {
    if (empty($key)) {
      return $this->global_index;
    }
    return isset($this->group_index[$key]) ? $this->group_index[$key] : 0;
  }

  /**
   * Returns a row.
   */
  public function &get_row($key = NULL) {
    if (empty($key)) {
      $key = $this->index_map[$this->get_index()];
    }
    return $this->rows[$key];
  }

  /**
   * If a key is provided this returns TRUE if the keyed row is first in it's
   * parent's group or not. If a key is not provided this returns TRUE if the
   * row is first in the whole view.
   */
  public function is_first($key = NULL) {
    return $this->get_index($key) == 0;
  }

  /**
   * If a key is provided this returns TRUE if the keyed row is of an odd index
   * in it's parent's group or not. If a key is not provided this returns TRUE
   * if the row of an odd index in the whole view.
   */
  public function is_odd($key = NULL) {
    return !($this->get_index($key) % 2);
  }

  /**
   * Moves the index one step forward.
   */
  public function move_index($key = NULL) {
    if (empty($key)) {
      $this->global_index++;
    }
    else {
      $this->group_index[$key]++;
    }
  }

  /**
   * Sets the properties for one row.
   *
   * @todo
   *   Is it cleaner to pass in a complete style object here?
   */
  public function prepare_row($key, $style_name = NULL, $parent = NULL) {
    $this->rows[$key] = array(
      'key' => $key,
      'style_name' => $style_name,
      'parent' => $parent,
    );
    $this->index_map[] = $key;
  }

  /**
   * Render the output for one row.
   */
  public function render_row($content = NULL) {
    $row = $this->get_row();
    $group_prefix = '';
    $group_suffix = '';

    // If we doesn't have a global style object, and we have an active parent
    // field, that means that all parent shall open a new group.
    if (empty($this->options['style_name']) && !empty($this->options['parent_field'])) {
      $group_key = $row['parent'];
      if (empty($row['parent'])) {
        $group_prefix = $row['style']['group']['prefix'];
        $group_key = $row['key'];
      }
      if ($this->last[$group_key] == $row['key']) {
        $group_suffix = $row['style']['group']['suffix'];
      }
    }

    return $group_prefix . $row['style']['prefix'] . $content . $row['style']['suffix'] . $group_suffix;
  }

  /**
   * Render the output of all rows.
   */
  public function render_rows($rows = array()) {
    // From now on we will only render rows with the global index in mind.
    // So we must reset that index to be able to render all rows.
    $this->reset_index();

    $output = '';
    foreach ($rows as $content) {
      $output .= $this->render_row($content);
      $this->move_index();
    }
    return $output;
  }

  /**
   * Resets the index back to zero.
   */
  public function reset_index($key = NULL) {
    if (empty($key)) {
      $this->global_index = 0;
    }
    else {
      $this->group_index[$key] = 0;
    }
  }
}
